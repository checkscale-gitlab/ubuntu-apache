# Pull base image
FROM ubuntu:xenial

# Install apache
RUN \
  apt-get update && \
  apt-get install -y apache2 && \
  rm -rf /var/lib/apt/lists/*

# Copy index to document root
COPY index.html /var/www/html/index.html

# Expose HTTP Port
EXPOSE 80

# Run apachectl
ENTRYPOINT ["/usr/sbin/apachectl"]
CMD ["-D", "FOREGROUND"]
